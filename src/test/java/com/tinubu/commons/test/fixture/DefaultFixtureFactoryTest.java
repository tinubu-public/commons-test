package com.tinubu.commons.test.fixture;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.tinubu.commons.test.fixture.junit.FixtureExtension;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectA;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectAFixtureFactory;

@ExtendWith(FixtureExtension.class)
class DefaultFixtureFactoryTest {

   @Test
   public void testDefaultFixtureFactoryWhenCreate() {
      ObjectA testObject = new ObjectAFixtureFactory().create();

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(0);
   }

   @Test
   public void testDefaultFixtureFactoryWhenCreateWithIndex() {
      ObjectA testObject = new ObjectAFixtureFactory().create(1);

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(1);
   }

   @Test
   public void testDefaultFixtureFactoryWhenCreateWithOutOfBoundIndex() {
      assertThatThrownBy(() -> new ObjectAFixtureFactory().create(100))
            .isInstanceOf(IndexOutOfBoundsException.class)
            .hasMessage("Index out of range: 100");
   }

   @Test
   public void testDefaultFixtureFactoryWhenCreateList() {
      List<ObjectA> testObjects = new ObjectAFixtureFactory().createList(3);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects)
            .extracting(ObjectA::index)
            .containsExactlyElementsOf(IntStream.range(0, 3).boxed().collect(toList()));
   }

   @Test
   public void testDefaultFixtureFactoryWhenCreateMap() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectAFixtureFactory().createMap(3, (Function<ObjectA, Object>) null))
            .withMessage("'keyIndexer' must not be null");

      Map<Integer, ObjectA> testObjects = new ObjectAFixtureFactory().createMap(3, ObjectA::index);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(IntStream.range(0, 3).boxed().toArray(Integer[]::new));
      assertThat(testObjects.values())
            .extracting(ObjectA::index)
            .containsExactlyElementsOf(IntStream.range(0, 3).boxed().collect(toList()));
   }

   @Test
   public void testDefaultFixtureFactoryWhenCreateMapWithIndexedIndexer() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectAFixtureFactory().createMap(3,
                                                                    (BiFunction<Integer, ObjectA, Object>) null))
            .withMessage("'keyIndexer' must not be null");

      Map<Integer, ObjectA> testObjects = new ObjectAFixtureFactory().createMap(3, (i, o) -> o.index());

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(IntStream.range(0, 3).boxed().toArray(Integer[]::new));
      assertThat(testObjects.values())
            .extracting(ObjectA::index)
            .containsExactlyElementsOf(IntStream.range(0, 3).boxed().collect(toList()));
   }

   @Test
   public void testDefaultFactoryFixturePrefixIndexString() {
      assertThat(new ObjectAFixtureFactory().prefixIndexString("string", 0)).isEqualTo("string");
      assertThat(new ObjectAFixtureFactory().prefixIndexString("string", 0, ",")).isEqualTo("string");
      assertThat(new ObjectAFixtureFactory().prefixIndexString("string", 1)).isEqualTo("1 string");
      assertThat(new ObjectAFixtureFactory().prefixIndexString("string", 1, ",")).isEqualTo("1,string");
   }

   @Test
   public void testDefaultFactoryFixturePostfixIndexString() {
      assertThat(new ObjectAFixtureFactory().postfixIndexString("string", 0)).isEqualTo("string");
      assertThat(new ObjectAFixtureFactory().postfixIndexString("string", 0, ",")).isEqualTo("string");
      assertThat(new ObjectAFixtureFactory().postfixIndexString("string", 1)).isEqualTo("string 1");
      assertThat(new ObjectAFixtureFactory().postfixIndexString("string", 1, ",")).isEqualTo("string,1");
   }

   @Test
   public void testDefaultFactoryFixtureFormatIndexString() {
      assertThat(new ObjectAFixtureFactory().formatIndexString("str%ding", 0)).isEqualTo("str0ing");
      assertThat(new ObjectAFixtureFactory().formatIndexString("str%ding", 1)).isEqualTo("str1ing");
      assertThat(new ObjectAFixtureFactory().formatIndexString("string", 1)).isEqualTo("string");
   }

}