package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(ObjectA.class)
public class ObjectAFixtureFactory extends DefaultFixtureFactory<ObjectA> {

   public ObjectA create(int index) {
      if (index > 99) throw new IndexOutOfBoundsException("Index out of range: " + index);
      return new ObjectA(index);
   }
}
