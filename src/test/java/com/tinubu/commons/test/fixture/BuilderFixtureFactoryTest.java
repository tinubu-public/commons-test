package com.tinubu.commons.test.fixture;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import com.tinubu.commons.test.fixture.junit.FixtureExtension;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectCFixtureFactory;

@ExtendWith(FixtureExtension.class)
class BuilderFixtureFactoryTest {

   @Test
   public void testBuilderFixtureFactoryWhenBuild() {
      ObjectC testObject = new ObjectCFixtureFactory().build();

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(0);
   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildWithIndex() {
      ObjectC testObject = new ObjectCFixtureFactory().build(1);

      assertThat(testObject).isNotNull();
      assertThat(testObject.index()).isEqualTo(1);
   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildWithOutOfBoundIndex() {
      assertThatThrownBy(() -> new ObjectCFixtureFactory().build(100))
            .isInstanceOf(IndexOutOfBoundsException.class)
            .hasMessage("Index out of range: 100");
   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildList() {
      List<ObjectC> testObjects = new ObjectCFixtureFactory().buildList(3);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects)
            .extracting(ObjectC::index)
            .containsExactlyElementsOf(IntStream.range(0, 3).boxed().collect(toList()));
   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildListWithOutOfBoundIndex() {
      assertThatThrownBy(() -> new ObjectCFixtureFactory().buildList(101))
            .isInstanceOf(IndexOutOfBoundsException.class)
            .hasMessage("Index out of range: 100");
   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildMap() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3, (Function<ObjectC, Object>) null))
            .withMessage("'keyIndexer' must not be null");

      Map<Integer, ObjectC> testObjects = new ObjectCFixtureFactory().buildMap(3, ObjectC::index);

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(0, 1, 2);
      assertThat(testObjects.values()).extracting(ObjectC::index).containsExactly(0, 1, 2);

   }

   @Test
   public void testBuilderFixtureFactoryWhenBuildMapWithIndexedIndexer() {
      assertThatNullPointerException()
            .isThrownBy(() -> new ObjectCFixtureFactory().buildMap(3,
                                                                   (BiFunction<Integer, ObjectC, Object>) null))
            .withMessage("'keyIndexer' must not be null");

      Map<Integer, ObjectC> testObjects = new ObjectCFixtureFactory().buildMap(3, (i, o) -> o.index());

      assertThat(testObjects).isNotNull();
      assertThat(testObjects).containsKeys(0, 1, 2);
      assertThat(testObjects.values()).extracting(ObjectC::index).containsExactly(0, 1, 2);
   }

}