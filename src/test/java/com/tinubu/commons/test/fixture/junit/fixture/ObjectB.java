package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.lang.model.Builder;

public class ObjectB {

   private int index;

   public ObjectB(int index) {
      this.index = index;
   }

   public int index() {
      return index;
   }

   public static class ObjectBBuilder implements Builder<ObjectB> {

      private int index = 0;

      public ObjectBBuilder index(int index) {
         this.index = index;
         return this;
      }

      @Override
      public ObjectB build() {
         return new ObjectB(index);
      }
   }

}
