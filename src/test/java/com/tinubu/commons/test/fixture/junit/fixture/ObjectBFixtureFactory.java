package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.test.fixture.DefaultFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;

@FixtureFactory(ObjectB.class)
public class ObjectBFixtureFactory extends DefaultFixtureFactory<ObjectB> {

   public ObjectB create(int index) {
      if (index > 99) throw new IndexOutOfBoundsException("Index out of range: " + index);
      return new ObjectB(index);
   }
}
