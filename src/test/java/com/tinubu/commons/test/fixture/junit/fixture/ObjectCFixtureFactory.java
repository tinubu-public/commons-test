package com.tinubu.commons.test.fixture.junit.fixture;

import com.tinubu.commons.test.fixture.AdvancedBuilderFixtureFactory;
import com.tinubu.commons.test.fixture.junit.FixtureFactory;
import com.tinubu.commons.test.fixture.junit.fixture.ObjectC.ObjectCBuilder;

@FixtureFactory(value = ObjectCBuilder.class, buildClass = ObjectC.class)
public class ObjectCFixtureFactory extends AdvancedBuilderFixtureFactory<ObjectC, ObjectCBuilder> {

   public ObjectCBuilder create(int index) {
      return new ObjectCBuilder().index(index);
   }
}
