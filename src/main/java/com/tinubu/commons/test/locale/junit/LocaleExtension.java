/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.test.locale.junit;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.util.Optional;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;

/**
 * Extension to configure default locale in each test using {@link Locale} annotation.
 * <p>
 * If {@link Locale} annotation is not found, the system locale is not updated
 * Annotate your test method or test class with {@link Locale} annotation to configure the required locale.
 * <p>
 * Declare this JUnit extension with {@link ExtendWith}.
 */
public class LocaleExtension implements BeforeAllCallback, BeforeEachCallback {

   private static final Logger logger = LoggerFactory.getLogger(LocaleExtension.class);

   private static final Locale DEFAULT_LOCALE = null;

   @Override
   public void beforeAll(ExtensionContext context) {
      setLocale(context);
   }

   @Override
   public void beforeEach(ExtensionContext context) {
      setLocale(context);
   }

   private void setLocale(ExtensionContext context) {
      localeConfig(context).ifPresent(localeConfig -> {
         final java.util.Locale locale =
               new java.util.Locale(localeConfig.language(), localeConfig.country(), localeConfig.variant());
         java.util.Locale.setDefault(locale);
         logger.info("Set system locale to '{}'", locale);
      });
   }

   private Optional<Locale> localeConfig(ExtensionContext context) {
      return nullable(context
                            .getTestMethod()
                            .map(testMethod -> AnnotationUtils.findAnnotation(testMethod, Locale.class))
                            .orElseGet(() -> context
                                  .getTestClass()
                                  .map(testClass -> AnnotationUtils.findAnnotation(testClass, Locale.class))
                                  .orElse(DEFAULT_LOCALE)));
   }

}
