/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.test.timezone.junit;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;

import java.time.ZoneId;
import java.util.Optional;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;

/**
 * Extension to configure default time zone in each test using {@link TimeZone} annotation.
 * <p>
 * If {@link TimeZone} annotation is not found, the system timezone is not updated.
 * Annotate your test method or test class with {@link TimeZone} annotation to configure the required time
 * zone.
 *
 *
 * <p>
 * Declare this JUnit extension with {@link ExtendWith}.
 */
public class TimeZoneExtension implements BeforeAllCallback, BeforeEachCallback {

   private static final Logger logger = LoggerFactory.getLogger(TimeZoneExtension.class);

   private static final TimeZone DEFAULT_TIMEZONE = null;

   @Override
   public void beforeAll(ExtensionContext context) {
      setTimezone(context);
   }

   @Override
   public void beforeEach(ExtensionContext context) {
      setTimezone(context);
   }

   private void setTimezone(ExtensionContext context) {
      timeZoneConfig(context).ifPresent(timeZoneConfig -> {
         ZoneId timeZone = ZoneId.of(timeZoneConfig.value());
         java.util.TimeZone.setDefault(java.util.TimeZone.getTimeZone(timeZone));
         logger.info("Set system time zone to '{}'", timeZone);
      });
   }

   private Optional<TimeZone> timeZoneConfig(ExtensionContext context) {
      return nullable(context
                            .getTestMethod()
                            .map(testMethod -> AnnotationUtils.findAnnotation(testMethod, TimeZone.class))
                            .orElseGet(() -> context
                                  .getTestClass()
                                  .map(testClass -> AnnotationUtils.findAnnotation(testClass, TimeZone.class))
                                  .orElse(DEFAULT_TIMEZONE)));
   }

}
