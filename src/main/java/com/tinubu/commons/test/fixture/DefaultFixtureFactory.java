package com.tinubu.commons.test.fixture;

import static com.tinubu.commons.lang.validation.Validate.notNull;

/**
 * Default fixture factory implementation.
 */
public abstract class DefaultFixtureFactory<T> implements FixtureFactory<T> {

   private static final String DEFAULT_INDEX_SEPARATOR = " ";

   /**
    * Returns postfixed {@code str} with the specified {@code separator} and index only if index &gt; 0.
    *
    * @param str string to postfix
    * @param index index value
    * @param separator separator between string and postfix
    *
    * @return postfixed string
    */
   protected String postfixIndexString(String str, int index, String separator) {
      notNull(str, "str");
      notNull(separator, "separator");

      if (index == 0) return str;
      else return str + separator + index;
   }

   /**
    * Returns postfixed {@code str} with {@value DEFAULT_INDEX_SEPARATOR} and index only if index &gt; 0.
    *
    * @param str string to postfix
    * @param index index value
    *
    * @return postfixed string
    */
   protected String postfixIndexString(String str, int index) {
      return postfixIndexString(str, index, DEFAULT_INDEX_SEPARATOR);
   }

   /**
    * Returns prefixed {@code str} with index and the specified {@code separator} only if index &gt; 0.
    *
    * @param str string to prefix
    * @param index index value
    * @param separator separator between string and prefix
    *
    * @return prefixed string
    */
   protected String prefixIndexString(String str, int index, String separator) {
      notNull(str, "str must not be null");
      notNull(separator, "separator must not be null");
      if (index == 0) return str;
      else return index + separator + str;
   }

   /**
    * Returns prefixed {@code str} with index and {@value DEFAULT_INDEX_SEPARATOR} only if index &gt; 0.
    *
    * @param str string to prefix
    * @param index index value
    *
    * @return prefixed string
    */
   protected String prefixIndexString(String str, int index) {
      return prefixIndexString(str, index, DEFAULT_INDEX_SEPARATOR);
   }

   /**
    * Returns string formatted with {@link String#format(String, Object...) string format}-compatible
    * pattern.
    * The pattern should contain one and only one {@code %d} placeholder for index, or the format string will
    * be returned unmodified.
    *
    * @param strFormat string format
    * @param index index value
    *
    * @return formatted string.
    */
   protected String formatIndexString(String strFormat, int index) {
      return String.format(strFormat, index);
   }

}
