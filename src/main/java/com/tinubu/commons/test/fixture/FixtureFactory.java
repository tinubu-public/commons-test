package com.tinubu.commons.test.fixture;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.IntStream;

import com.tinubu.commons.lang.util.Pair;

/**
 * Fixture factory interface.
 * Fixture factories generates fixture objects.
 * <p>
 * Fixtures should follow these rules :
 * <ul>
 *    <li>Keep fixtures simple and "nominal"</li>
 *    <li>Use "good" values for data or self explaining values (A street = "street", ...), *not* funny data.
 *         Make integer values start from 0 or 1 for locally unique values instead of random values</li>
 *    <li>*Never* change a fixture to test a new use case, instead adapt the generated fixture locally in your test,
 *        so that :<ul>
 *        <li>fixture will keep working for all other tests</li>
 *        <li>you we'll clearly show the test specific context data in your test</li></ul></li>
 *     <li>Adapt your fixture data to specified index to generate fixture collections easily</li>
 *     <li>Support generically an unlimited number of index values or it will indirectly limit other fixtures count.
 *         Only throws an {@link IndexOutOfBoundsException} if it is effectively impossible to generate more indexed fixtures</li>
 *     <li>Do *not* try to simulate all possible data combinations with indexed fixtures, instead, adapt fixture
 *         data in your tests</li>
 *     <li>At least, ensure fixture object uniqueness rules are fulfilled when generating indexed data</li>
 *     <li>For immutable objects, make fixture factory generate object builder so that you have a chance to adapt
 *         object data in your tests</li>
 *     <li>The entire graph of referenced fixture objects for a given domain should be consistent, i.e a fixture factory
 *         implementation must call referenced objects own fixtures, propagating the current index or not, depending on logic,
 *         to generate the fixture</li>
 * </ul>
 */
public interface FixtureFactory<T> {

   /**
    * Generates a fixture using provided index.
    *
    * @param index fixture generator index
    *
    * @return a new fixture
    *
    * @throws IndexOutOfBoundsException if no fixture can be generated with specified index
    */
   T create(int index) throws IndexOutOfBoundsException;

   /**
    * Generates a new fixture.
    * Alias to {@link #create(int) create(0)}.
    *
    * @return a new fixture
    */
   default T create() {
      return create(0);
   }

   /**
    * Generates a list of fixtures of specified size.
    *
    * @param count number of fixture elements to generate
    *
    * @return a list of fixtures
    *
    * @throws IndexOutOfBoundsException if not enough fixtures can be generated for specified count
    */
   default List<T> createList(int count) {
      return list(IntStream.range(0, count).mapToObj(((Function<Integer, T>) this::create)::apply));
   }

   /**
    * Generates a map of fixtures of specified size using the function to index the keys.
    *
    * @param <V> key transformer type
    * @param count number of fixture elements to generate
    * @param keyIndexer function used to identify the key of the map
    *
    * @return fixture map
    *
    * @throws IndexOutOfBoundsException if not enough fixtures can be generated for specified count
    */
   default <V> Map<V, T> createMap(int count, Function<? super T, ? extends V> keyIndexer) {
      notNull(keyIndexer, "keyIndexer");

      return createList(count).stream().collect(toMap(keyIndexer, identity()));
   }

   /**
    * Generates a map of fixtures of specified size using the function to index the keys.
    *
    * @param <V> key transformer type
    * @param count number of fixture elements to generate
    * @param keyIndexer function used to identify the key of the map. First parameter is index
    *
    * @return fixture map
    *
    * @throws IndexOutOfBoundsException if not enough fixtures can be generated for specified count
    */
   default <V> Map<V, T> createMap(int count, BiFunction<Integer, ? super T, ? extends V> keyIndexer) {
      notNull(keyIndexer, "keyIndexer");

      List<T> fixtures = createList(count);

      return map(IntStream
                       .range(0, count)
                       .mapToObj(i -> Pair.of(keyIndexer.apply(i, fixtures.get(i)), fixtures.get(i))));
   }

}
