package com.tinubu.commons.test.fixture.junit;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.platform.commons.util.AnnotationUtils;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.filter.AbstractClassTestingTypeFilter;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;

import com.tinubu.commons.test.fixture.BuilderFixtureFactory;

/**
 * Extension to support fixture injection in tests using {@link Fixture} annotation.
 * <p>
 * By default, the configured base package to scan for fixture factories is {@link
 * FixtureConfig#basePackages()}.
 * Annotate your test method or test class with {@link FixtureConfig} annotation to configure this extension.
 * <p>
 * Declare this JUnit extension with {@link ExtendWith}.
 */
public class FixtureExtension implements ParameterResolver, AfterEachCallback {

   private static final Object STORED_INJECTION_COUNT = "injection-count";
   private static final FixtureConfig DEFAULT_FIXTURE_CONFIG =
         DefaultFixtureConfigHolder.defaultFixtureConfig();

   /** Scanned factories cache indexed by base package. */
   private static final Map<String, List<Class<?>>> fixtureFactories = new ConcurrentHashMap<>();

   @Override
   public void afterEach(ExtensionContext context) {
      // TODO clean the object store for this test. requires different store namespacing.
   }

   @Override
   public boolean supportsParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
      return parameterContext.getParameter().isAnnotationPresent(Fixture.class);
   }

   /**
    * Resolves fixture parameters.
    */
   @Override
   public Object resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) {
      Class<?> fixtureClass = fixtureClass(parameterContext);

      FixtureConfig fixtureConfig = fixtureConfig(extensionContext);

      @SuppressWarnings("squid:S1872" /* Classes should not be compared by name */)
      List<Class<?>> fixtureFactories = fixtureFactories(basePackages(fixtureConfig))
            .stream()
            .filter(directFixtureFactory(fixtureClass).or(builderFixtureFactory(fixtureClass)))
            .collect(toList());

      if (fixtureFactories.size() == 0) {
         throw new ParameterResolutionException(String.format(
               "No fixture factory found for object '%s' with '%s' config",
               fixtureClass,
               fixtureConfig));
      } else if (fixtureFactories.size() > 1) {
         throw new ParameterResolutionException(String.format(
               "Found multiple fixture factories for '%s' with '%s' config : %s",
               fixtureClass,
               fixtureConfig,
               fixtureFactories.stream().map(Class::getName).collect(joining(", "))));
      } else {
         return resolveFixture(fixtureClass, fixtureFactories.get(0), parameterContext, extensionContext);
      }
   }

   private Predicate<Class<?>> directFixtureFactory(Class<?> fixtureClass) {
      return factory -> {
         Optional<FixtureFactory> fixtureFactory =
               AnnotationUtils.findAnnotation(factory, FixtureFactory.class);

         return fixtureFactory.map(ffc -> ffc.value().equals(fixtureClass)).orElse(false);
      };
   }

   private Predicate<Class<?>> builderFixtureFactory(Class<?> fixtureClass) {
      return factory -> {
         if (BuilderFixtureFactory.class.isAssignableFrom(factory)) {
            Optional<FixtureFactory> fixtureFactory =
                  AnnotationUtils.findAnnotation(factory, FixtureFactory.class);

            return fixtureFactory
                  .map(ffc -> nullable(ffc.buildClass())
                        .map(bc -> bc.length == 1 && bc[0].equals(fixtureClass))
                        .orElse(false))
                  .orElse(false);
         } else {
            return false;
         }
      };
   }

   private List<String> basePackages(FixtureConfig fixtureConfig) {
      return Stream.of(fixtureConfig.basePackages()).collect(toList());
   }

   private FixtureConfig fixtureConfig(ExtensionContext extensionContext) {
      return extensionContext
            .getTestMethod()
            .flatMap(testMethod -> AnnotationUtils.findAnnotation(testMethod, FixtureConfig.class))
            .orElseGet(() -> extensionContext
                  .getTestClass()
                  .flatMap(testClass -> AnnotationUtils.findAnnotation(testClass, FixtureConfig.class))
                  .orElseGet(() -> extensionContext
                        .getTestClass()
                        .flatMap(tc -> nullable(tc.getEnclosingClass()))
                        .flatMap(testClass -> AnnotationUtils.findAnnotation(testClass, FixtureConfig.class))
                        .orElse(DEFAULT_FIXTURE_CONFIG)));
   }

   private Object resolveFixture(Class<?> fixtureClass,
                                 Class<?> fixtureFactoryClass,
                                 ParameterContext parameterContext,
                                 ExtensionContext extensionContext) {

      com.tinubu.commons.test.fixture.FixtureFactory<?> fixtureFactory;
      try {
         fixtureFactory = (com.tinubu.commons.test.fixture.FixtureFactory<?>) fixtureFactoryClass
               .getDeclaredConstructor()
               .newInstance();
      } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException |
               InstantiationException e) {
         throw new ParameterResolutionException(e.getMessage(), e);
      }

      Fixture fixtureConfig = parameterContext.getParameter().getAnnotation(Fixture.class);
      Object fixture;

      boolean buildFixture = builderFixtureFactory(fixtureClass).test(fixtureFactoryClass);

      if (parameterContext.getParameter().getType().isAssignableFrom(List.class)) {
         if (buildFixture) {
            fixture = ((BuilderFixtureFactory<?, ?>) fixtureFactory).buildList(fixtureConfig.count());
         } else {
            fixture = fixtureFactory.createList(fixtureConfig.count());
         }
      } else {
         Store injectionStore = getInjectionStore(parameterContext, extensionContext, fixtureClass);
         int injectionCount = injectionStore.getOrComputeIfAbsent(STORED_INJECTION_COUNT, __ -> 0, int.class);

         if (buildFixture) {
            fixture = ((BuilderFixtureFactory<?, ?>) fixtureFactory).build();
         } else {
            fixture = fixtureFactory.create(injectionCount);
         }

         injectionStore.put(STORED_INJECTION_COUNT, injectionCount + 1);
      }

      return fixture;
   }

   private static List<Class<?>> fixtureFactories(List<String> basePackages) {
      Set<Class<?>> fixtures = new HashSet<>();

      for (String basePackage : basePackages) {
         List<Class<?>> packageFixtures = fixtureFactories.get(basePackage);

         if (packageFixtures == null) {
            ClassPathScanningCandidateComponentProvider scanner =
                  new ClassPathScanningCandidateComponentProvider(false);
            scanner.addIncludeFilter(new AssignableTypeFilter(com.tinubu.commons.test.fixture.FixtureFactory.class));
            scanner.addIncludeFilter(new AnnotationTypeFilter(FixtureFactory.class));
            scanner.addExcludeFilter(new AbstractClassTestingTypeFilter() {
               @Override
               protected boolean match(ClassMetadata metadata) {
                  return !metadata.isConcrete();
               }
            });

            packageFixtures = scanner.findCandidateComponents(basePackage).stream().map(def -> {
               try {
                  return requireNonNull(Thread
                                              .currentThread()
                                              .getContextClassLoader()).loadClass(def.getBeanClassName());
               } catch (ClassNotFoundException e) {
                  throw new IllegalStateException(e);
               }
            }).collect(toList());

            fixtureFactories.put(basePackage, packageFixtures);
         }

         fixtures.addAll(packageFixtures);
      }

      return list(stream(fixtures).sorted(Comparator.comparing(Class::getName)));
   }

   /**
    * Returns the real parameter class. In case of 'List<X>' we want 'X' and not 'List'.
    */
   private Class<?> fixtureClass(ParameterContext parameterContext) {
      Class<?> clazz;
      if (parameterContext.getParameter().getType().isAssignableFrom(List.class)) {
         ParameterizedType parameterizedType =
               (ParameterizedType) parameterContext.getParameter().getParameterizedType();
         clazz = (Class<?>) parameterizedType.getActualTypeArguments()[0];
      } else {
         clazz = parameterContext.getParameter().getType();
      }

      return clazz;
   }

   /**
    * Gets or create store for (this extension, test method, fixture class).
    *
    * @param parameterContext parameter context
    * @param extensionContext extension context
    * @param fixtureClass injected fixture class
    *
    * @return store
    */
   private Store getInjectionStore(ParameterContext parameterContext,
                                   ExtensionContext extensionContext,
                                   Class<?> fixtureClass) {

      String testMethod = testMethod(parameterContext).getName();

      return extensionContext.getStore(Namespace.create(getClass(), testMethod, fixtureClass));
   }

   private Method testMethod(ParameterContext parameterContext) {
      Executable declaringExecutable = parameterContext.getDeclaringExecutable();
      if (declaringExecutable instanceof Method) {
         return (Method) declaringExecutable;
      } else {
         throw new IllegalStateException("@Fixture must be used in test methods parameters");
      }
   }

   /** Holds the default {@link FixtureConfig} instance. */
   @FixtureConfig
   private static class DefaultFixtureConfigHolder {
      private static FixtureConfig defaultFixtureConfig() {
         return DefaultFixtureConfigHolder.class.getAnnotation(FixtureConfig.class);
      }
   }
}
